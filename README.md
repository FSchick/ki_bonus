# KI_Bonus
Um die Versionen der Bibliothenken einheitlich zu halten, sollten wir alle anaconda benutzen und das file "enviorment.yml" updaten, insofern eine neue Bilbliothek zu der Umgebung hinzugefügt wird. 

Um die aktuelle Version der Umgebung zu laden muss zunächst diese Repository geladen werden. Anschließend kann die Umgebung mit den folgenden Befehlen erstellt und aktiviert werden werden.
```bash
conda env create -f environment.yml 
conda activate KI_Bonus
```


Über den folgenden Befehl kann sie deaktiviert werden.
```bash
conda deactivate
```

Wenn die Umgebung verändert wird können die Änderungen über diesen Befehl in die Datei geschrieben werden. 
```bash
conda env export > environment.yml
```


Aktuell sind die folgenden Bibliothenken installiert:
-numpy
-pandas
-matplotlib
-scikit
-tensorlow
-jupyter notebook

Die beinhalten Links sind mögliche Datensets / Aufgaben <br/>
https://www.kaggle.com/c/ashrae-energy-prediction <br/>
https://www.kaggle.com/valentynsichkar/traffic-signs-preprocessed <br/>
https://www.kaggle.com/c/house-prices-advanced-regression-techniques <br/>
https://www.kaggle.com/c/ieee-fraud-detection
